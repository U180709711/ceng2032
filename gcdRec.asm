L: 
add $a0,$s0,$zero   # put first number in $a0 as input argument
add $a1,$s1,$zero   # put second number in $a1 as input argument



jal GCD

GCD:

addi $sp,$sp,-4
sw $ra,0($sp)


bne $a1,$zero,L1  # base case condition

add $v0,$zero,$a0  # if base case is true
addi $sp,$sp,4
jr $ra


L1:
div $a0,$a1
add $a0,$a1,$zero
mfhi $a1
jal GCD

lw $ra,0($sp)
addi $sp,$sp,4

jr $ra

add $s3,$v0,$zero  # put result in $s3

li $v0,4
la $a0,string3
syscall

li $v0,1
add $a0,$s3,$zero
syscall